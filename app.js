const express = require('express'),
     morgan = require('morgan'),
     path = require('path');

const app = express(),
     port = 3000;

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname,'views/pages'));

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname,'public')));
app.use((req,res,next) => {
     console.log(req.url);
     next();
});
app.use('/', require('./routes/Authorization'));
app.use('/', require('./routes/UserRatings'));

app.listen(port, () => {
     console.log('server started on localhost:' + port);
});