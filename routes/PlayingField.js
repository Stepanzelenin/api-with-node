const express = require('express'),
    router = express.Router();

router.get('/PlayingField', (req,res) => {
    res.render('playing_field')
})

module.exports = router;