const express = require('express'),
    router = express.Router();
    fs = require('fs');
    bcrypt = require('bcrypt');

router.get('/Auth', (req,res) => {
    res.render('auth')
})    

router.post('/Auth', (req, res) => {
  const { login, password } = req.body;
  const users = fs.readFileSync('users.txt', 'utf-8').trim().split('\n');
  const user = users.find((user) => user.split(':')[0] === login);

  if (user) {
    const hashedPassword = user.split(':')[1];

    bcrypt.compare(password, hashedPassword, (err, result) => {
      if (err) {
        console.error(err);
        res.status(500).send('Internal server error');
      } else if (result) {
        res.redirect('/Rating');
      } else {
        res.status(401).send('Invalid username or password');
      }
    });
  } else {
    res.status(401).send('Invalid username or password');
  }
});

module.exports = router;