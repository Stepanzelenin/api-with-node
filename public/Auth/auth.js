const loginButton = document.querySelector('.button');
const loginField = document.getElementById('login-field');
const passwordField = document.getElementById('password-field');

loginField.addEventListener('copy', (event) => event.preventDefault());
passwordField.addEventListener('copy', (event) => event.preventDefault());

loginField.addEventListener('input', () => {
   loginField.value = loginField.value.replace(/[^a-zA-Z0-9._]/g, '');
});

passwordField.addEventListener('input', () => {
   passwordField.value = passwordField.value.replace(/[^a-zA-Z0-9._]/g, '');
});

loginButton.addEventListener('click', () => {
   if (loginField.value === '' || passwordField.value === '') {
     console.error('Логин и пароль должны быть заполнены.');
     alert("Логин и пароль должны быть заполнены.")
     return;
   }

   fetch('/Auth', {
     method: 'POST',
     headers: {
       'Content-Type': 'application/json'
     },
     body: JSON.stringify({
       login: loginField.value,
       password: passwordField.value
     })
   })
   .then(res => {
     if (res.redirected) {
       window.location.href = res.url;
     } else {
       console.error('Неверное имя пользователя или пароль.');
       alert("Неверное имя пользователя или пароль.")
     }
   })
   .catch(err => console.error(err));
});